<?php 

namespace App\Traits;

use Illuminate\Support\Str;

trait UsesUuid {

  public static function boot()
  {
    parent::boot();
    self::creating(function ($model) {
        $model->id = Str::orderedUuid()->toString();
    });
  }

  public function getIncrementing()
  {
      return false;
  }

  public function getKeyName()
  {
      return 'id';
  }

  /**
   * Get the auto-incrementing key type.
   *
   * @return string
   */
  public function getKeyType()
  {
      return 'string';
  }
}