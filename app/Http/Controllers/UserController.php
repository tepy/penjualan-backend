<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class UserController extends Controller
{
  
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function getUser(Request $req)
  {
    return response()->json([
      'code' => 0,
      'desc' => 'success',
      'data' => [
          'data' => User::all(), 
          'user' => Auth::user()
        ]
      ], 200);
  }

  public function refresh()
  {
    return $this->respondWithToken(auth()->refresh());
  }

}