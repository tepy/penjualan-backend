<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    
    $validator = Validator::make($request->all(), [
        'name' => 'required|string',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed',
    ]);
    
    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }
    try {

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $plainPassword = $request->input('password');
        $user->password = app('hash')->make($plainPassword);

        $user->save();

        return response()->json(['code'=> 0, 'desc' => 'Success create user', 'data' => $user], 201);

    } catch (\Exception $e) {
        return response()->json(['code'=>-1, 'desc' => 'User Registration Failed!'], 409);
    }
  }

  public function login(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'email' => 'required|string',
        'password' => 'required|string',
    ]);
    if ($validator->fails()) {
        return response()->json($validator->errors(), 402);
    }

    $credentials = $request->only(['email', 'password']);

    if (! $token = Auth::attempt($credentials)) {
        return response()->json(['code' => -1, 'desc' => 'account not registered check your credential'], 401);
    }

    return $this->respondWithToken($token);
  }

  public function refresh(Request $req)
  {
    return $this->respondWithToken(auth()->refresh());
  }

  public function logout()
  {
    auth()->logout();
    return response()->json([
      'code'        => 0, 
      'response_code'=> 200,  
      'desc'        => 'Successfully logged out'
    ], 200);
  }


}