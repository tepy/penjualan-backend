<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;
use Validator;

class StatistikController extends Controller
{
  public function getDataStatistic(Request $req)
  {
    $validator = Validator::make($req->all(), [
      'date'=> 'required|string',
    ]);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }
    
    $dt_date = explode("|", $req->date);

    $dateStart = date("Y-m-d", strtotime(str_replace('/', '-', $dt_date[0])));
    $dateFinish = date("Y-m-d", strtotime(str_replace('/', '-', $dt_date[1])));

    $data = DB::table("orders AS t1");
    $data->leftJoin("order_details AS t2", "t1.id", "=","t2.order_id");
    $data->select([DB::raw("sum(subtotal) AS total"), DB::raw("date_format(t1.date,'%Y%m%d') AS tgl")]);
    $data->where("t1.f_data", 1)->where("t2.f_data", 1);
    $data->whereBetween(DB::raw("date_format(t1.date, '%Y-%m-%d')"), [$dateStart, $dateFinish]);
    $data->groupBy("tgl");
    
    return response()->json([
      'code' => 0, 
      'desc' => 'success', 
      'data' => [
        'data' => $data->get(), 
        'date' => [
                "start" => $dateStart , 
                "end" => $dateFinish
              ],
      ] 
    ], 200);
  }
}
