<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use Validator;
use Illuminate\Support\Facades\Crypt;

class OrderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  public function datatable(Request $req)
  {
    $dt = request()->post();
    $total = app('db')->select('select count(*) as total from orders where f_data = 1')[0]->total;

    $db = DB::table('orders');
    $having = "";
    if ($dt['search']['value'] != "") {
      $keywords = explode(" ", $dt['search']['value']);
      $keywords = 
        array_map(function($x){
          return trim($x);
        }, $keywords);
      $keywords =
        array_filter($keywords, function($x){
          return strlen($x) > 0;
        });
      for ($i = 0; $i < count($keywords); $i++) {
        if($keywords[$i] != ' ' && $keywords[$i] != '%' && $keywords[$i] != ''){
          $having .= ' keywords LIKE \'%'.$keywords[$i].'%\' ';
          if ($i < count($keywords) - 1) {
            $having .= ' AND ';
          }
        }
      }
    }
    if ($having != "") {
      $db->havingRaw($having);
    }
    $db->select(["buyer_name", "order_number", "id", "date", "f_data", DB::raw("concat_ws('|', buyer_name, order_number, id, date_format(date, '%M %Y %d') ) as keywords")])->where('f_data', 1);
    $tot = $db->get()->count();
    
    $colOrder = $dt['columns'][$dt['order'][0]['column']]['data'];
    if ($colOrder == 0) {
      $db->orderBy('updated_at', 'desc');
    } else {
      $db->orderBy($colOrder, $dt['order'][0]['dir']);
    }

    if ($dt['start'] != ""){
      $db->offset($dt['start']);
    }
    if ($dt['length'] != ""){
      $db->limit($dt['length']);
    }
    $value = $db->get();
    
    $data = [];
    $no = $dt['start'] + 1;
    foreach ($value as $val) {
      $data[] = [
        'id'    => $no++,
        'buyer_name'  => $val->buyer_name,
        'order_number'=> $val->order_number,
        'date'  => date('d F Y', strtotime($val->date)),
        'act'   => "<button type=\"\" data-payload=\"" . Crypt::encrypt($val->id) . "\" class=\"btn btn-sm btn-secondary btn-edit btn-datatable-order\">Edit</button> <button type=\"\" data-payload=\"" . $val->id . "\" class=\"btn btn-sm btn-danger btn-delete btn-datatable-order\">Hapus</button>",
      ];
    }
    $obj = [
      "draw"            => $dt["draw"],
      "recordsTotal"    => $total,
      "recordsFiltered" => $tot,
      'data'            => $data
    ];
    return response()->json($obj, 200, [], JSON_PRETTY_PRINT);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  
  public function store(Request $req)
  {
    $validator = Validator::make($req->all(), [
      'buyer_name'  => 'required|string|max:65',
      'order_number' => 'required|string|max:55',
      'date' => 'required|date',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    try {
      $obj = new Order;
      $obj->buyer_name = $req->buyer_name;
      $obj->order_number = $req->order_number;
      $obj->date = $req->date;
      $obj->updated_at = Carbon::now();
      $obj->f_data = 1;
      $obj->save();
      return response()->json(['code' => 0, 'desc' => 'success', 'data' => $obj->fresh() ], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = Order::findOrFail(Crypt::decrypt($id));
    return response()->json(['code' => 0, 'desc' => 'success', 'data' => $data], 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, $id)
  {
    $validator = Validator::make($req->all(), [
      'buyer_name'  => 'bail|required|string|max:65',
      'order_number'=> 'bail|required|string|max:55',
      'date'        => 'bail|required|date',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    try {
      $obj = Order::findOrFail($id);
      $obj->buyer_name = $req->buyer_name;
      $obj->order_number = $req->order_number;
      $obj->date = $req->date;
      $obj->updated_at = Carbon::now();
      $obj->f_data = 1;
      $obj->save();
      return response()->json(['code' => 0, 'desc' => 'success', 'data' => $obj->fresh()], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      Order::findOrFail($id)->update([
        'f_data' => -1, 
        'updated_at' => Carbon::now(),
      ]);
      return response()->json(['code' => 0, 'desc' => 'success'], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }
  }
}
