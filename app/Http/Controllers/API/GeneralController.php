<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;

class GeneralController extends Controller
{
  public function index()
  {
    return response()->json(['code' => 0, 'desc' => app()->version()], 200, [], JSON_PRETTY_PRINT);
  }

  public function optimasiDatabase(Request $req)
  {
    $dt = app('db')->select('optimize table order_details, users, orders, items');
    return response()->json($dt);
  }

}
