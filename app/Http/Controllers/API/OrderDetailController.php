<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\OrderDetail;
use App\Models\Item;
use Illuminate\Support\Facades\DB;
use Validator;

class OrderDetailController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  public function datatable(Request $req)
  {
    $dt = request()->post();
    $total = app('db')->select('select count(*) as total from order_details where f_data = 1 and order_id = "' . 
      $req->order_id .'"' )[0]->total;

    $db = DB::table('order_details as t1')->leftJoin('items as t2', 't2.id', '=', 't1.item_id');
    $having = "";
    if ($dt['search']['value'] != "") {
      $keywords = explode(" ", $dt['search']['value']);
      $keywords = 
        array_map(function($x){
          return trim($x);
        }, $keywords);
      $keywords =
        array_filter($keywords, function($x){
          return strlen($x) > 0;
        });
      for ($i = 0; $i < count($keywords); $i++) {
        if($keywords[$i] != ' ' && $keywords[$i] != '%' && $keywords[$i] != ''){
          $having .= ' keywords LIKE \'%'.$keywords[$i].'%\' ';
          if ($i < count($keywords) - 1) {
            $having .= ' AND ';
          }
        }
      }
    }
    if ($having != "") {
      $db->havingRaw($having);
    }
    $db->select(["t2.name", "t1.price", "t1.qty", "t1.id", "t1.subtotal", "t1.f_data", DB::raw("concat_ws('|', t2.name, t1.qty ) as keywords")])->where([['t1.f_data', '=', '1'],['t1.order_id', '=', $req->order_id]]);
    $tot = $db->get()->count();

    $colOrder = $dt['columns'][$dt['order'][0]['column']]['data'];
    if ($colOrder == 0) {
      $db->orderBy('t1.updated_at', 'desc');
    } else {
      $db->orderBy($colOrder, $dt['order'][0]['dir']);
    }

    if ($dt['start'] != ""){
      $db->offset($dt['start']);
    }
    if ($dt['length'] != ""){
      $db->limit($dt['length']);
    }

    $value = $db->get();
    
    $data = [];
    $no = $dt['start'] + 1;
    foreach ($value as $val) {
      $data[] = [
        'id'      => $no++,
        'name'    => $val->name,
        'price'   => 'Rp ' . number_format($val->price, 0, ',', '.'),
        'qty'     => $val->qty,
        'subtotal'=> 'Rp ' . number_format($val->subtotal, 0, ',', '.'),
        'act'     => "<div style=\"white-space: nowrap;\"><button type=\"\" data-payload=\"" . $val->id . "\" class=\"btn btn-sm btn-secondary btn-edit btn-datatable-order-item\">Edit</button> <button type=\"\" data-payload=\"" . $val->id . "\" class=\"btn btn-sm btn-danger btn-delete btn-datatable-order-item\">Hapus</button></div>"
      ];
    }
    $obj = [
      "draw"            => $dt["draw"],
      "recordsTotal"    => $total,
      "recordsFiltered" => $tot,
      'data'            => $data
    ];
    return response()->json($obj, 200, [], JSON_PRETTY_PRINT);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $req)
  {

    $validator = Validator::make($req->all(), [
      'order_id'=> 'bail|required|string|min:36',
      'item_id' => 'bail|required|string|min:36',
      'qty'     => 'bail|required|integer',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    try {
      $item = Item::findOrFail($req->item_id);
      $obj = new OrderDetail;
      $obj->order_id = $req->order_id;
      $obj->item_id = $item->id;
      $obj->price = intval($item->price);
      $obj->qty = $req->qty;
      $obj->subtotal = $req->qty * intval($item->price);
      $obj->f_data = 1;
      $obj->created_at = Carbon::now();
      $obj->updated_at = Carbon::now();
      $obj->save();
      return response()->json(['code' => 0,'desc' => 'success'], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1,'desc' => $e], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = OrderDetail::findOrFail($id);
    return response()->json(['code' => 0, 'desc' => 'success', 'data' => $data], 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, $id)
  {

    $validator = Validator::make($req->all(), [
      'order_id'=> 'bail|required|string|min:36',      
      'item_id' => 'bail|required|string|min:36',
      'qty'     => 'bail|required|integer',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    try {
      $item = Item::findOrFail($req->item_id);
      $obj = OrderDetail::findOrFail($id);
      $obj->order_id = $req->order_id;
      $obj->item_id = $req->item_id;
      $obj->price = intval($item->price);
      $obj->qty = $req->qty;
      $obj->subtotal = $req->qty * intval($item->price);
      $obj->updated_at = Carbon::now();
      $obj->save();
      return response()->json(['code' => 0,'desc' => 'success', 'data' => $obj->fresh()], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1,'desc' => $e], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      OrderDetail::findOrFail($id)->update([
        'updated_at' => Carbon::now(),
        'f_data' => -1,
      ]);
      return response()->json(['code' => 0, 'desc' => 'success'], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }
  }
}
