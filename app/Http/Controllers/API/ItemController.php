<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Validator;

class ItemController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = Item::where('f_data', 1)->get();
    return response()->json([
      'code' => 0,
      'desc'=> 'success',
      'data' => $data,
    ], 200, [], JSON_PRETTY_PRINT);
  }

  public function datatable(Request $req) {
    $dt = request()->post();
    $total = app('db')->select('select count(*) as total from items where f_data = 1')[0]->total;

    $db = DB::table('items');
    $having = "";
    if ($dt['search']['value'] != "") {
      $keywords = explode(" ", $dt['search']['value']);
      $keywords = 
        array_map(function($x){
          return trim($x);
        }, $keywords);
      $keywords =
        array_filter($keywords, function($x){
          return strlen($x) > 0;
        });
      for ($i = 0; $i < count($keywords); $i++) {
        if($keywords[$i] != ' ' && $keywords[$i] != '%' && $keywords[$i] != ''){
          $having .= ' keywords LIKE \'%'.$keywords[$i].'%\' ';
          if ($i < count($keywords) - 1) {
            $having .= ' AND ';
          }
        }
      }
    }
    if ($having != "") {
      $db->havingRaw($having);
    }
    $db->select(["name", "price", "id", "stock", "f_data", DB::raw("concat_ws('|', name, price, id, stock ) as keywords")])->where('f_data', 1);
    $tot = $db->get()->count();

    $colOrder = $dt['columns'][$dt['order'][0]['column']]['data'];
    if ($colOrder == 0) {
      $db->orderBy('created_at', 'desc');
    } else {
      $db->orderBy($colOrder, $dt['order'][0]['dir']);
    }
    
    if ($dt['start'] != ""){
      $db->offset($dt['start']);
    }
    if ($dt['length'] != ""){
      $db->limit($dt['length']);
    }
    $value = $db->get();
    
    $data = [];
    $no = $dt['start'] + 1;
    foreach ($value as $val) {
      $data[] = [
        'id'    => $no++,
        'name'  => $val->name,
        'price' => 'Rp ' . number_format($val->price, 0, ',', '.'),
        'stock' => number_format($val->stock, 0, ',', '.'),
        'act'   => "<button type=\"\" data-payload=\"" . $val->id . "\" class=\"btn btn-sm btn-secondary btn-edit btn-datatable-item\">Edit</button> <button type=\"\" data-payload=\"" . $val->id . "\" class=\"btn btn-sm btn-danger btn-delete btn-datatable-item\">Hapus</button>",
      ];
    }
    $obj = [
      "draw"            => $dt["draw"],
      "recordsTotal"    => $total,
      "recordsFiltered" => $tot,
      'data'            => $data,
      'req'             => $req->all()
    ];
    return response()->json($obj, 200, [], JSON_PRETTY_PRINT);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $req)
  {
    $validator = Validator::make($req->all(), [
      'name'  => 'required|string|max:150',
      'price' => 'required|integer',
      'stock' => 'required|integer',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    $obj = new Item;
    $obj->name = $req->name;
    $obj->price = $req->price;
    $obj->stock = $req->stock;
    $obj->created_by = Auth::user()->id;
    $obj->created_at = Carbon::now();
    $obj->updated_at = Carbon::now();
    $obj->f_data = 1;
    
    try {
      $obj->save();
      return response()->json(['code' => 0, 'desc' => 'success'], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }


  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = Item::findOrFail($id);
    return response()->json(['code' => 0, 'desc' => 'success', 'data' => $data], 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, $id)
  {
    $validator = Validator::make($req->all(), [
      'name'  => 'required|string|max:150',
      'price' => 'required|integer',
      'stock' => 'required|integer',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 402);
    }

    $obj = Item::find($id);
    $obj->name = $req->name;
    $obj->price = $req->price;
    $obj->stock = $req->stock;
    $obj->created_by = Auth::user()->id;
    $obj->updated_at = Carbon::now();
    
    try {
      $obj->save();
      return response()->json(['code' => 0, 'desc' => 'success', 'data'=> $obj->refresh()], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      Item::findOrFail($id)->update(['f_data' => -1, 'updated_at' => Carbon::now()]);
      return response()->json(['code'=> 0, 'desc' => 'data berhasil dihapus'], 200);
    } catch (Exception $e) {
      return response()->json(['code' => -1, 'desc' => $e], 400);
    }
  }
}
