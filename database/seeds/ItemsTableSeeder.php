<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Item;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ItemsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    if (Item::all()->count() != 0) {
      Item::truncate();
    }

    $faker = Factory::create('id_ID');
    for ($i=0; $i < 20; $i++) { 
      $user = User::inRandomOrder()->first();
      Item::create([
        'name'      => $faker->colorName() . ' ' .$faker->colorName(),
        'price'     => $faker->randomDigitNotNull() * 3500,
        'stock'     => $faker->randomDigitNotNull() * 6,
        'created_by'=> $user->id,
        'f_data'    => 1,
        'created_at'=> Carbon::now(),
        'updated_at'=> Carbon::now(),
      ]);
    }
  }
}
