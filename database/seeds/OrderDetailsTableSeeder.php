<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;

class OrderDetailsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    if (OrderDetail::all()->count() != 0) {
      OrderDetail::truncate();
    }
    $faker = Factory::create('id_ID');
    for ($i=0; $i < 5000; $i++) { 
      $item = Item::inRandomOrder()->first();
      $order = Order::inRandomOrder()->first();
      $qty = $faker->randomDigitNotNull();
      OrderDetail::create([
        'order_id'=> $order->id,
        'item_id' => $item->id,
        'price'   => intval($item->price),
        'qty'     => $qty,
        'subtotal'=> $qty * intval($item->price),
        'f_data'  => 1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }
    
  }
}
