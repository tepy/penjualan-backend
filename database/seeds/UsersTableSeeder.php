<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    if (User::all()->count() != 0) {
      User::truncate();
    }

    $users = [
      [
        'name'      =>'admin',
        'email'     => 'admin@mail.com',
        'password'  => app('hash')->make('password'),
        'created_at'=> Carbon::now(),
        'updated_at'=> Carbon::now(),
      ]
    ];
    
    foreach ($users as $user) {
      User::create($user);
    }

  }
}
