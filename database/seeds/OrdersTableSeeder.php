<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;

class OrdersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    if (Order::all()->count() != 0) {
      Order::truncate();
    }
    $faker = Factory::create('id_ID');
    for ($i=0; $i < 1000; $i++) { 
      $user = User::inRandomOrder()->first();
      Order::create([
        'buyer_name'  => $faker->name(),
        'order_number'=> $faker->swiftBicNumber(),
        'date'        => $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now', $timezone = null)->format('Y-m-d'),
        'created_by'  => $user->id,
        'f_data'      => 1,
        'created_at'  => Carbon::now(),
        'updated_at'  => Carbon::now(),
      ]); 
    }
  }
}
