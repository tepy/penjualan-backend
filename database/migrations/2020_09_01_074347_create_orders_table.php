<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->uuid('id')->unique()->primary();
            $table->string('buyer_name', 65);
            $table->string('order_number', 55);
            $table->uuid('created_by');
            $table->date('date')->nullable(true)->index();
            $table->smallInteger('f_data')->nullable(false)->default(0);
            $table->timestamps();
            $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
