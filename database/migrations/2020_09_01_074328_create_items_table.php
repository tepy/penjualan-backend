<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->uuid('id')->unique()->primary();
            $table->string('name', 150);
            $table->decimal('price', 12, 2)->nullable(false)->default(0);
            $table->integer('stock')->nullable(false)->default(0);
            $table->uuid('created_by');
            $table->smallInteger('f_data')->nullable(false)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
