<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->uuid('id')->unique()->primary();
            $table->uuid('order_id');
            $table->uuid('item_id');
            $table->decimal('price', 12, 2)->nullable(false)->default(0);
            $table->integer('qty')->nullable(false)->default(0);
            $table->decimal('subtotal', 12, 2)->nullable(false)->default(0);
            $table->smallInteger('f_data')->nullable(false)->default(0);
            $table->timestamps();
            $table->index('order_id');
            $table->index('item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
