<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', 'API\GeneralController@index');

$router->group(['prefix' => 'api'], function () use ($router) {
   
  $router->post('register', 'AuthController@register');
  $router->post('login', 'AuthController@login');

  $router->group(['middleware' => 'auth'], function() use ($router) {
    
    $router->post('refresh', 'AuthController@refresh');
    $router->post('logout', 'AuthController@logout');
    $router->post('optimasi-database', 'API\GeneralController@optimasiDatabase');

    $router->get('/get-user', 'UserController@getUser');
    $router->get('/get-refresh', 'UserController@refresh');

    $router->group(['prefix' => '/item'], function() use ($router) {
      $router->get('/list-item', 'API\ItemController@index');
      $router->post('/datatable', 'API\ItemController@datatable');
      $router->post('/create', 'API\ItemController@store');
      $router->get('/{id}/show', 'API\ItemController@show');
      $router->post('/{id}/update', 'API\ItemController@update');
      $router->post('/{id}/delete', 'API\ItemController@destroy');
    });

    $router->group(['prefix' => '/order'], function() use ($router) {
      $router->post('/datatable', 'API\OrderController@datatable');
      $router->post('/create', 'API\OrderController@store');
      $router->get('/{id}/show', 'API\OrderController@show');
      $router->post('/{id}/update', 'API\OrderController@update');
      $router->post('/{id}/delete', 'API\OrderController@destroy');
    });

    $router->group(['prefix' => '/order-detail'], function() use ($router) {
      $router->post('/datatable', 'API\OrderDetailController@datatable');
      $router->post('/create', 'API\OrderDetailController@store');
      $router->get('/{id}/show', 'API\OrderDetailController@show');
      $router->post('/{id}/update', 'API\OrderDetailController@update');
      $router->post('/{id}/delete', 'API\OrderDetailController@destroy');
    });

    $router->group(['prefix' => '/statistik'], function() use ($router) {
      $router->post('/get-data', 'API\StatistikController@getDataStatistic');
    });

  });

});
